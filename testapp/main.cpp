#include "libbattlelog.h"
#include <iostream>


int main()
{
    auto filter = LBL::CreateFilter();
    filter->setType(LBL::TYPE_OFFICIAL);
    filter->setType(LBL::TYPE_RANKED);
    filter->setGameMode(LBL::GAMEMODE_TEAMDM);
    filter->setGameMode(LBL::GAMEMODE_SQUADDM);
    filter->setSlots(LBL::SLOTS_1TO5);
    filter->setSlots(LBL::SLOTS_6TO10);

    LBL::BLSession session;
    LBL::IServerInfo *pickedServer = nullptr;

    auto servers = session.getServers(filter.get());
    for (const auto& server : servers)
    {
        printf("gameId: %016llX\n", server->getGameId());
        printf("ip: %s port: %i\n", server->getIP(), server->getPort());

        for (const auto& team : server->getTeams())
            printf("teamNr: %i | teamsPts: %i | teamMaxPts: %i\n", team.number, team.points, team.maxPoints);

        pickedServer = server.get();
    }

    std::string email, password;
    printf("enter your email: \n");
    std::cin >> email;
    printf("enter your password: \n");
    std::cin >> password;

    if (!session.login(email, password))
        printf("login failed\n");

    auto soldierId = session.getSoldierId();
    printf("soldierId: %s\n", soldierId.c_str());

    auto authCode = session.getAuthCode(soldierId);
    printf("authCode: %s\n", authCode.c_str());

    if (pickedServer)
    {
        session.reserveSlot(pickedServer, soldierId);
        session.launchGame(pickedServer, authCode, soldierId);
    }

    std::cin.ignore();
    std::cin.ignore();
}