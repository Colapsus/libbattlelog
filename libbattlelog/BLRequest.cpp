#include "BLRequest.h"


BLRequest::BLRequest(const std::string& method, const std::string& uri)
    : Poco::Net::HTTPRequest(method, uri)
{
    setVersion(Poco::Net::HTTPRequest::HTTP_1_1);
    setKeepAlive(true);
    // chrome 34 beta
    add("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.92 Safari/537.36");
    add("Referer", "http://battlelog.battlefield.com/bf4/");
    add("Accept", "*/*");
    add("Accept-Encoding", "gzip,deflate");
    add("Accept-Language", "de,en-US;q=0.7,en;q=0.3");
}