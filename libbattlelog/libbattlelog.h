#ifndef LIBBATTLELOG_H
#define LIBBATTLELOG_H

#include <string>
#include <cstdint>
#include <vector>
#include <memory>


#ifdef _MSC_VER
#ifdef BATTLELOG_EXPORT
#define BATTLELOG_API __declspec(dllexport)
#else
#define BATTLELOG_API __declspec(dllimport)
#endif
#else
#define BATTLELOG_API
#endif


namespace LBL
{
    /**
    \brief represents a game server
    */
    class BATTLELOG_API IServerInfo
    {
    public:
        struct Team
        {
            int number = 0;
            int points = 0;
            int maxPoints = 0;
        };

        virtual ~IServerInfo() { }

        virtual std::uint64_t getGameId() const = 0;
        virtual const char *getIP() const = 0;
        virtual std::uint16_t getPort() const = 0;
        virtual std::vector<Team> getTeams() const = 0;
    };


    enum GameMode
    {
        GAMEMODE_CONQUEST = 0x1,
        GAMEMODE_RUSH = 0x2,
        GAMEMODE_SQUADDM = 0x8,
        GAMEMODE_TEAMDM = 0x20,
        GAMEMODE_CONQUESTLARGE = 0x40,
        GAMEMODE_DOMINATION = 0x400,
        GAMEMODE_OBLITERATION = 0x200000,
        GAMEMODE_DEFUSE = 0x1000000,
        GAMEMODE_CTF = 0x80000,
        GAMEMODE_AIRSUPERIORITY = 0x800000,
        GAMEMODE_CALARGE = 0x4000000,
        GAMEMODE_CA = 0x8000000,
    };
    enum Slots
    {
        SLOTS_NONE = 0x10,
        SLOTS_1TO5 = 0x1,
        SLOTS_6TO10 = 0x2,
        SLOTS_10PLUS = 0x4,
        SLOTS_ALL = 0x8,
        SLOTS_COMMANDER = 0x10000,
        SLOTS_SPECTATOR = 0x20000,
    };
    enum Region
    {
        REGION_NA = 0x1,
        REGION_SA = 0x2,
        REGION_ANTARCTICA = 0x4,
        REGION_AFRICA = 0x8,
        REGION_EUROPE = 0x10,
        REGION_ASIA = 0x20,
        REGION_OCEANIA = 0x40,
    };
    enum Preset
    {
        PRESET_NORMAL = 0x1,
        PRESET_HARDCORE = 0x2,
        PRESET_INFANTRY = 0x4,
    };
    enum Type
    {
        TYPE_OFFICIAL = 0x1,
        TYPE_RANKED = 0x2,
        TYPE_UNRANKED = 0x4,
        TYPE_PRIVATE = 0x8,
    };


    /**
    \brief A filter used for searching servers

    Create a new filter with CreateFilter.

    Although the enums are bitfields, they may not be combined! Use two seperate calls to the set* member functions.
    */
    class BATTLELOG_API IFilter
    {
    public:
        virtual ~IFilter() { }

        virtual void setSearchTerm(const std::string& term) = 0;
        virtual void setGameMode(GameMode mode) = 0;
        virtual void setSlots(Slots slots) = 0;
        virtual void setRegion(Region region) = 0;
        virtual void setPreset(Preset preset) = 0;
        virtual void setType(Type type) = 0;
        virtual void setSize(int size) = 0;
    };

    std::unique_ptr<IFilter> BATTLELOG_API CreateFilter();


    class BATTLELOG_API BLSession
    {
    public:
        BLSession();
        ~BLSession();

        /**
        \brief retrieves servers from battlelog
        \return A vector of IServerInfo

        Does not require login.
        */
        std::vector<std::unique_ptr<IServerInfo>> getServers(const IFilter *filter);

        /**
        \brief login to battlelog/origin
        \return true if successful

        The cookie will be stored internally and used with all requests that need it.
        */
        bool login(const std::string& email, const std::string& password);

        /**
        \brief gets your own soldier id
        \return soldierId

        Requires login.
        */
        std::string getSoldierId();

        /**
        \brief request an authcode that is used to login to the game
        \return authCode

        Requires login.
        */
        std::string getAuthCode(const std::string& soldierId);

        /**
        \brief reserves a slot on the server

        Requires login.
        */
        void reserveSlot(const IServerInfo *server, const std::string& soldierId);

        /**
        \brief starts the game
        \param server The server you want to join on
        \param authCode The login token that can be retrieved with LBL::getAuthCode
        \param soldierId The soldier you want to join with

        Requires login and LBL::reserveSlot.
        */
        void launchGame(const IServerInfo *server, const std::string& authCode, const std::string& soldierId);

    private:
        class BLSessionImpl;
        BLSessionImpl *m_impl;
    };
}

#endif