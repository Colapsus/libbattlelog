#ifndef BLREQUEST_H
#define BLREQUEST_H

#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTMLForm.h"


class BLRequest : public Poco::Net::HTTPRequest
{
public:
    // Creates a HTTP/1.1 request with the given method and URI.
    BLRequest(const std::string& method, const std::string& uri);

    std::ostream& sendForm(Poco::Net::HTMLForm& form);

private:

};

#endif