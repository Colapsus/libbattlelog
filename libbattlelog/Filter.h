#ifndef FILTER_H
#define FILTER_H

#include "libbattlelog.h"
#include "Poco/Net/HTMLForm.h"


class Filter : public LBL::IFilter
{
public:
    void setSearchTerm(const std::string& term) override
    {
        m_searchTerm = term;
    }
    void setGameMode(LBL::GameMode mode) override
    {
        m_gameModes.push_back(mode);
    }
    void setSlots(LBL::Slots slots) override
    {
        m_slots.push_back(slots);
    }
    void setRegion(LBL::Region region) override
    {
        m_regions.push_back(region);
    }
    void setPreset(LBL::Preset preset) override
    {
        m_presets.push_back(preset);
    }
    void setType(LBL::Type type) override
    {
        m_types.push_back(type);
    }
    void setSize(int size) override
    {
        m_sizes.push_back(size);
    }

    std::unique_ptr<Poco::Net::HTMLForm> generateForm() const
    {
        auto form = std::make_unique<Poco::Net::HTMLForm>();

        form->add("filtered", "1");
        form->add("expand", "1");
        //form->add("settings", "vgmc,0,500"); when checked
        form->add("useLocation", "1");
        form->add("useAdvanced", "1");
        form->add("gameexpansions", "-1"); // all

        for (const auto& mode : m_gameModes)
            form->add("gamemodes", std::to_string(mode));

        // the slots use a bitfield but battlelog doesnt actually use them as one bitfield, but multiple form entries
        for (const auto& slots : m_slots)
        {
            if (slots & LBL::SLOTS_COMMANDER)
                form->add("commanderSlots", "1");
            else if (slots & LBL::SLOTS_SPECTATOR)
                form->add("spectatorSlots", "1");
            else
                form->add("slots", std::to_string(slots));
        }

        for (const auto& size : m_sizes)
            form->add("gameSize", std::to_string(size));

        form->add("q", m_searchTerm);

        for (const auto& type : m_types)
            form->add("serverTypes", std::to_string(type));

        for (const auto& preset : m_presets)
            form->add("gamepresets", std::to_string(preset));

        form->add("mapRotation", "-1");
        form->add("modeRotation", "-1");
        form->add("password", "-1");

        for (const auto& region : m_regions)
            form->add("regions", std::to_string(region));

        form->add("osls", "-1");
        form->add("vvsa", "-1");
        form->add("vffi", "-1");
        form->add("vaba", "-1");
        form->add("vkca", "-1");
        form->add("v3ca", "-1");
        form->add("v3sp", "-1");
        form->add("vmsp", "-1");
        form->add("vrhe", "-1");
        form->add("vhud", "-1");
        form->add("vmin", "-1");
        form->add("vnta", "-1");
        form->add("vbdm-min", "1");
        form->add("vbdm-max", "300");
        form->add("vprt-min", "1");
        form->add("vprt-max", "300");
        form->add("vshe-min", "1");
        form->add("vshe-max", "300");
        form->add("vtkk-min", "1");
        form->add("vtkk-max", "99");
        form->add("vnit-min", "30");
        form->add("vnit-max", "86400");
        form->add("vtkc-min", "1");
        form->add("vtkc-max", "99");
        form->add("vvsd-min", "0");
        form->add("vvsd-max", "500");
        //form->add("settings-vgmc", "1"); // when checked
        form->add("vgmc-min", "0");
        form->add("vgmc-max", "500");

        return std::move(form);
    }

private:
    std::string m_searchTerm;
    std::vector<LBL::GameMode> m_gameModes;
    std::vector<LBL::Slots> m_slots;
    std::vector<LBL::Region> m_regions;
    std::vector<LBL::Preset> m_presets;
    std::vector<LBL::Type> m_types;
    std::vector<int> m_sizes;

};

#endif